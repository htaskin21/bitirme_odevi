﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.Veritabanı;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            KisileriYenile();
        }
        private void buttonEkle_Click(object sender, EventArgs e)
        {
            string ad, soyad, telefon;

            
            ad = textBoxAd.Text;
            soyad = textBoxSoyad.Text;
            telefon = textBoxTelefon.Text;

            

            if (!string.IsNullOrEmpty(ad) && !string.IsNullOrEmpty(soyad) && !string.IsNullOrEmpty(telefon)) 
            {
                
                Kisi yeniKisi = new Kisi();
                yeniKisi.Ad = ad;
                yeniKisi.Soyad = soyad;
                yeniKisi.Telefon = telefon;


                
                DataContext veritabanı = new DataContext(); 

                
                veritabanı.Kisiler.Add(yeniKisi);
                
                veritabanı.SaveChanges();

                
                KisileriYenile();
            }
            else 
            {
                MessageBox.Show("Tüm alanlar dolu olmalı.", "Kaydet", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void buttonGuncelle_Click(object sender, EventArgs e)
        {
            if (listBoxKisiler.SelectedIndex > -1) 
            {
                string ad, soyad, telefon;

                
                ad = textBoxAd.Text;
                soyad = textBoxSoyad.Text;
                telefon = textBoxTelefon.Text;


                
                if (!string.IsNullOrEmpty(ad) && !string.IsNullOrEmpty(soyad) && !string.IsNullOrEmpty(telefon)) 
                {
                    
                    string seçiliVeri = listBoxKisiler.SelectedItem.ToString();

                   
                    string[] adSoyadTelefon = seçiliVeri.Split('-');

                    string seciliAd, seciliSoyad, seciliTelefon;
                    seciliAd = adSoyadTelefon[0];
                    seciliSoyad = adSoyadTelefon[1];
                    seciliTelefon = adSoyadTelefon[2];

                   
                    DataContext veritabanı = new DataContext();

                    
                    Kisi guncellenecekKisi = veritabanı.Kisiler.Where(s => s.Ad == seciliAd && s.Soyad == seciliSoyad && s.Telefon == seciliTelefon).FirstOrDefault();
                   
                    if (guncellenecekKisi != null)
                    {
                        guncellenecekKisi.Ad = ad;
                        guncellenecekKisi.Soyad = soyad;
                        guncellenecekKisi.Telefon = telefon;

                        
                        veritabanı.SaveChanges();

                        
                        KisileriYenile();
                    }
                }
                else 
                {
                    MessageBox.Show("Tüm alanlar dolu olmalı.", "Kaydet", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
        }

        private void buttonSil_Click(object sender, EventArgs e)
        {

            if (listBoxKisiler.SelectedIndex > -1) 
            {

                
                string seçiliVeri = listBoxKisiler.SelectedItem.ToString();

                
                string[] adSoyadTelefon = seçiliVeri.Split('-');

                string seciliAd, seciliSoyad, seciliTelefon;
                seciliAd = adSoyadTelefon[0];
                seciliSoyad = adSoyadTelefon[1];
                seciliTelefon = adSoyadTelefon[2];

                
                DataContext veritabanı = new DataContext();

                
                Kisi silinecekKisi = veritabanı.Kisiler.Where(s => s.Ad == seciliAd && s.Soyad == seciliSoyad && s.Telefon == seciliTelefon).FirstOrDefault();
               
                if (silinecekKisi != null)
                {
                    
                    veritabanı.Kisiler.Remove(silinecekKisi);

                   
                    veritabanı.SaveChanges();

                    
                    KisileriYenile();
                }
            }
        }

        private void KisileriYenile()
        {
            listBoxKisiler.Items.Clear();
            DataContext veritabanı = new DataContext(); 
            List<Kisi> kisiler = veritabanı.Kisiler.ToList(); 

            foreach (Kisi item in kisiler) 
            {
                string veri = item.Ad + "-" + item.Soyad + "-" + item.Telefon;
                listBoxKisiler.Items.Add(veri); 
            }
        }

        private void textBoxAd_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
