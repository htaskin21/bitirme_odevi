﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.Veritabanı
{
    public class DataContext : DbContext 
    {
        public DataContext() : base("KisilerDB.sdf") 
        {

        }
        public DbSet<Kisi> Kisiler { get; set; }  
    }
    public class Kisi  
    {
        [Key] 
        public int Id { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Telefon { get; set; }
    }
}
